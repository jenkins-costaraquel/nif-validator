# NIF Validator in Python
https://docs.gitlab.com/ee/integration/jenkins.html


By: __César Freire__

## Build 

    docker build -t nif_validator .

## Local run

    docker run -d --name nif_validator -p 80:9046 costaraquel/nif_validator

## Jenkins Gitlab integration

* Create a gitlab token (Access token)
* Add jenkins gitlab plugin
* Create project freestyle
* Trigger build
raquel