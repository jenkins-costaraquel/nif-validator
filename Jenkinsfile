#!groovy

/*
* CI on jenkins
* 20230510 Raquel Costa
*/

pipeline {
    agent any
    environment {
        HOME="${env.WORKSPACE}"
    }

    stages {
        stage('Create Docker') {
        agent {
            docker {
                image 'python:slim'
                reuseNode true
            }
        }   
            steps {
                sh"""
                export PIP_DISABLE_PIP_VERSION_CHECK=1
                export PYTHONDONTWRITEBYTECODE=1
                export PYTHONUNBUFFERED=1
                export PATH="${PATH}:$HOME/.local/bin"
                pip install --user -r requirements.txt
                pip install --user -r requirements-test.txt
                """
            }
        }

        stage('Unit Tests') {
            agent {
            docker {
                image 'python:slim'
                reuseNode true
            }
        }   
            steps {
                sh 'python3 -m pytest --junitxml results.xml tests/test_nif_validator.py'
            }
            post {
                always {
                    archiveArtifacts artifacts: 'results.xml', fingerprint: true 
                    junit 'results.xml'
                }
            }
        }

        stage('Integration Tests') {
            agent {
            docker {
                image 'python:slim'
                reuseNode true
            }
        }   
            steps {
                echo "TODO Integration Tests"
            }
        }

        stage('UI tests') {
            agent {
            docker {
                image 'python:slim'
                reuseNode true
            }
        }   
            steps {
                echo "TODO UI tests"
            }
        }

        stage('Coverage Report') {
            agent {
            docker {
                image 'python:slim'
                reuseNode true
            }
        }   
            steps {
                sh 'python3 -m coverage run --source=. -m pytest tests'
                sh 'python3 -m coverage report -m'
                sh 'python3 -m coverage html'
            }
            post {
                always {
                    publishHTML(target: [
                    allowMissing: false,
                    alwaysLinkToLastBuild: false,
                    keepAll: true,
                    reportDir: 'htmlcov',
                    reportFiles: 'index.html',
                    reportName: 'CoverageReport'
                    ])
                }
            }
        }
        
        stage('PEP8 Verification') {
            agent {
            docker {
                image 'python:slim'
                reuseNode true
            }
        }   
            steps {
                sh 'python3 -m flake8 . --exclude site-packages --exit-zero'
                sh 'exit 0'
            }
        }

        stage('Cyclomatic Complexity') {
            agent {
            docker {
                image 'python:slim'
                reuseNode true
            }
        }   
            steps {
                sh 'python3 -m radon cc . -a -s --exclude site-packages'
            }
        }

        
    
        stage('Build') {
            steps {
                withCredentials(
                    [usernamePassword(credentialsId: 'dockerHub',
                    usernameVariable:'dockerID',
                     passwordVariable:'dockerHubPassword')]) {
                sh "docker build -t '${env.dockerID}'/nif_validator:latest ."
                    
                }  
            }
        }

        stage('Delivery') {
            steps {
                withCredentials(
                    [usernamePassword(credentialsId: 'dockerHub',
                    passwordVariable:'dockerHubPassword', 
                    usernameVariable:'dockerHubUser')]) {
                    sh "docker login -u '${env.dockerHubUser}' -p '${env.dockerHubPassword}'"
                    sh "docker push '${env.dockerHubUser}'/nif_validator:latest"
                }
            }
        }
    }
}
    

